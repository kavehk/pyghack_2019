export const ACTIONS = {
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT'
}

const login = (user) => ({
    type: ACTIONS.LOGIN,
    user
})

const logout = () => ({
    type: ACTIONS.LOGOUT
})

export default {
    login,
    logout
}
