import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Header, Segment, Table } from 'semantic-ui-react'

const BaseAdmin = ({ user }) => (
    <Segment basic padded textAlign="center">
        <Header>Welcome {user.name}</Header>
        <Header>Active Requests</Header>
        <Table>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Requester</Table.HeaderCell>
                    <Table.HeaderCell>Contact</Table.HeaderCell>
                    <Table.HeaderCell>Item</Table.HeaderCell>
                    <Table.HeaderCell />
                </Table.Row>
            </Table.Header>
            <Table.Body>
                <Table.Row>
                    <Table.Cell>John Doe</Table.Cell>
                    <Table.Cell>(217) 555-5555</Table.Cell>
                    <Table.Cell>Bagpack</Table.Cell>
                    <Table.Cell>
                        <Button content="Reject" color="red" />
                        <Button content="Approve" color="green" />
                    </Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>Jane Doe</Table.Cell>
                    <Table.Cell>(217) 555-5556</Table.Cell>
                    <Table.Cell>Washer</Table.Cell>
                    <Table.Cell>
                        <Button content="Reject" color="red" />
                        <Button content="Approve" color="green" />
                    </Table.Cell>
                </Table.Row>
            </Table.Body>
        </Table>
    </Segment>
)

BaseAdmin.propTypes = {
    user: PropTypes.shape({}).isRequired
}

const mapStateToProps = (state) => {
    console.log(state)
    return {
        user: state.getIn(['accounts', 'user']).toJS()
    }
}

const Admin = connect(mapStateToProps)(BaseAdmin)

export default Admin
