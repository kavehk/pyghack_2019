import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Container, Image, Segment } from 'semantic-ui-react'

import pageActions, { VIEWS } from '../actions/page'

import src1 from "../images/logo.png";

const BaseIndex = ({ updateView }) => (
    <Segment basic padded id={"landing"}>
        <Segment basic style={{"width":"25%", "float":"right"}}>
            <Image src={src1} size='large' centered />
        </Segment>
        <Container fluid textAlign="right" style={{"paddingTop": "1em", "paddingRight": "35em"}}>
            <Segment basic>
                <Button
                    primary
                    content="Request an Item"
                    icon="add"
                    onClick={() => { updateView(VIEWS.REQUEST) }}
                />
                <Button
                    primary
                    content="See List of Items in Need"
                    icon="list"
                    onClick={() => { updateView(VIEWS.ITEMS) }} />
                <Button
                    primary
                    content="Admin"
                    icon="sign in"
                    onClick={() => { updateView(VIEWS.LOGIN) }}
                />
            </Segment>
        </Container>
    </Segment>
)

BaseIndex.propTypes = {
    updateView: PropTypes.func.isRequired
}

const Index = connect(null, { updateView: pageActions.updateView })(BaseIndex)

export default Index
