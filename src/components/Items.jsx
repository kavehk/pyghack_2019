import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Card, Form, Grid, Icon, Image, Modal, Segment, Select } from 'semantic-ui-react'

import { admins as adminsFixture } from '../fixtures'

const locationOptions = [{
    key: '-',
    text: '---',
    value: ''
}].concat(
    adminsFixture.map(({ id, name }) => ({
        key: id,
        text: name,
        value: id
    }))
)

const BaseItems = ({ items }) => {
    const [donateItem, updateDonateItem] = React.useState(null)
    return (
        <Segment basic padded>
            <Grid columns={3} stackable textAlign="center">
                {items.map((item) => {
                    const { id, name, description, numberRequested, numberAvailable, image } = item
                    return (
                        <Grid.Column key={id}>
                            <Card>
                                <Image src={image} wrapped ui={false} />
                                <Card.Content>
                                    <Card.Header>
                                        {name}
                                    </Card.Header>
                                    <Card.Meta>
                                        <span>Requested: {numberRequested}</span>
                                        <span>Available: {numberAvailable}</span>
                                        {numberAvailable > numberRequested ?
                                            <Icon
                                                name="check circle outline"
                                                color="green"
                                                size="large"
                                            /> :
                                            <Icon
                                                name="warning circle"
                                                color="yellow"
                                                size="large"
                                            />}
                                    </Card.Meta>
                                    <Card.Description>
                                        {description}
                                    </Card.Description>
                                </Card.Content>
                                <Card.Content extra textAlign="center">
                                    <Button
                                        color='green'
                                        content="Donate"
                                        onClick={() => updateDonateItem(item)}
                                    />
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                    )
                })}
            </Grid>
            {donateItem ?
                <Modal open={!!donateItem} onClose={() => updateDonateItem(null)}>
                    <Modal.Header>Donate {donateItem.name}</Modal.Header>
                    <Modal.Content image>
                        <Image wrapped size='medium' src={donateItem.image} />
                        <Modal.Description>
                            <Segment basic>
                                {donateItem.description}
                            </Segment>
                            <Form>
                                <Form.Input
                                    fluid
                                    label='Quantity'
                                    type="number"
                                    steps={1}
                                    defaultValue={1}
                                />
                                <Form.Field
                                    control={Select}
                                    label={{ children: 'Drop-off Location' }}
                                    options={locationOptions}
                                />
                            </Form>
                        </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button onClick={() => { updateDonateItem(null) }}>
                            <Icon name='remove' /> Cancel
                        </Button>
                        <Button color='green'>
                            <Icon name='checkmark' /> Donate
                        </Button>
                    </Modal.Actions>
                </Modal> :
                null}
        </Segment>
    )
}

BaseItems.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({})).isRequired
}

const mapStateToProps = (state) => ({
    items: state.getIn(['items', 'items']).toJS()
})

const Items = connect(mapStateToProps)(BaseItems)

export default Items
