import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react'

import { admins as adminsFixture } from '../fixtures'
import accountsActions from '../actions/accounts'
import pageActions, { VIEWS } from '../actions/page'

const BaseLogin = ({ login, updateView }) => {
    const [username, updateUsername] = React.useState('')
    const [password, updatePassword] = React.useState('')
    const [error, updateError] = React.useState('')
    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Log-in as an Admin
                </Header>
                <Form size='large' error={!!error}>
                    <Segment stacked>
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='Username'
                            onChange={(e, { value }) => {
                                updateUsername(value)
                                updateError('')
                            }}
                        />
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                            onChange={(e, { value }) => {
                                updatePassword(value)
                                updateError('')
                            }}
                        />
                        {error ?
                            <Message error>
                                {error}
                            </Message> :
                            null}

                        <Button
                            primary
                            fluid
                            size='large'
                            disabled={!username || !password}
                            onClick={() => {
                                const user = adminsFixture.find(
                                    (admin) => admin.username === username
                                )
                                if (user) {
                                    login(user)
                                    updateView(VIEWS.ADMIN)
                                } else {
                                    updateError('Wrong username or password')
                                }
                            }}
                        >
                            Login
                        </Button>
                    </Segment>
                </Form>
            </Grid.Column>
        </Grid>
    )
}

BaseLogin.propTypes = {
    login: PropTypes.func.isRequired,
    updateView: PropTypes.func.isRequired
}

const Login = connect(
    null,
    {
        login: accountsActions.login,
        updateView: pageActions.updateView
    }
)(BaseLogin)

export default Login
