import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Dropdown, Container, Button, Checkbox, Form, Input } from 'semantic-ui-react'

import { submitRequest } from '../actions/items'
import '../styles/Request.less'

const categories = [
  { text: 'Bagpack', value: 'Bagpack' },
  { text: 'Dryer', value: 'Dryer' },
  { text: 'Fridge', value: 'Fridge' }, 
  { text: 'Closet', value: 'Closet'  }, 
  { text: 'School Stationery', value: 'School Stationery' }, 
  { text: 'Washer', value: 'Washer' }
]

function addNew(e) { 
  e.preventDefault();
  console.log("i clicked");
}

class RequestPage extends React.Component { 
  constructor(props) { 
    super(props);
    this.state = { 
      numItems: 1
    };


  }

  addItem() { 
    // e.preventDefault();
    console.log('here');
    console.log(this.state.numItems);
   
    this.setState({ numItems: this.state.numItems + 1 }); 
    console.log(this.state.numItems);
  }

  render() { 
    const children = []
    for (let i = 0; i < this.state.numItems; i++) { 
      children.push((
          <Form.Group key={i}>
            <Form.Field width={10}>
              <label>Item</label>
              <Form.Dropdown
                placeholder='Select item category'
                fluid
                selection
                options={categories}
              />
            </Form.Field>
            <Form.Field>
              <label>Number</label>
              <Input placeholder='Number' />
            </Form.Field>
            <Form.Field>
              <label>&nbsp;</label>
              <Button fluid onClick={ () => this.addItem() }>
                Add
              </Button>
            </Form.Field>
          </Form.Group>
      ));
    }

    return (
    <Container>
      <div>
        <Form className='main'> 
          <Form.Group widths='equal'>
            <Form.Field> 
              <label>First Name</label>
              <Input placeholder='First Name' />
            </Form.Field>
            <Form.Field> 
              <label>Last Name</label>
              <Input placeholder='Last Name' />
            </Form.Field>
          </Form.Group>
          <Form.Field>
            <label>Phone Number</label>
            <Input placeholder='Phone Number' />
          </Form.Field>
          <Form.Field>
            <label>Address</label>
            <Input placeholder='Address' />
          </Form.Field>
          { children } 
          {/*
          <Form.Group>
            <Form.Field width={10}>
              <label>Item</label>
              <Form.Dropdown
                placeholder='Select item category'
                fluid
                selection
                options={categories}
              />
            </Form.Field>
            <Form.Field>
              <label>Number</label>
              <Input placeholder='Number' />
            </Form.Field>
            <Form.Field>
              <label>&nbsp;</label>
              <Button fluid onClick={addNew}>
                Add
              </Button>
            </Form.Field>
          </Form.Group>
          */ } 
          <Form.Checkbox label='I agree to the Terms and Conditions' />
          <Button type='submit'>Submit</Button>
        </Form>
      </div>
    </Container>
    )
  }
}

/** 
const BaseRequest = ({ submit }) => (
)

BaseRequest.prototype = {
    submit: PropTypes.func.isRequired
}
const Request = connect(null, { submit: submitRequest })(BaseRequest)
*/

export default RequestPage
