import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import { combineReducers } from 'redux-immutable'
import thunk from 'redux-thunk'

import baseReducers from './reducers'
import App from './components/App'

const render = (reducers = baseReducers) => {
    const composeEnhancers = (
        (process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose
    )

    const store = createStore(combineReducers(reducers), composeEnhancers(applyMiddleware(thunk)))

    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('root')
    )
}

render()
