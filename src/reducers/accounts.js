import { fromJS } from 'immutable'

import { ACTIONS } from '../actions/accounts'

const INITIAL_STATE = fromJS({
    user: null
})

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case ACTIONS.LOGIN:
            return state.set('user', fromJS(action.user))
        case ACTIONS.LOGOUT:
            return state.set('user', null)
        default:
            return state
    }

}
