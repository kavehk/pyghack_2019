import accounts from './accounts'
import items from './items'
import page from './page'

export default {
    accounts,
    items,
    page
}
